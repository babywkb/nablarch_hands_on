<%-- ProjectClassをimportする --%>
<%@page import="com.nablarch.example.app.web.common.code.ProjectClass"%>
<%-- ProjectTypeをimportする --%>
<%@page import="com.nablarch.example.app.web.common.code.ProjectType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="n" uri="http://tis.co.jp/nablarch" %>
<%@ page session="false" %>

<!DOCTYPE html>
<html>
    <head>
        <%-- javascript --%>
        <n:script type="text/javascript" src="/javascripts/lib/jquery-1.11.2.min.js"></n:script>
        <n:script type="text/javascript" src="/javascripts/projectInput.js"></n:script>
        <n:script type="text/javascript" src="/javascripts/clientList.js"></n:script>
        <title>プロジェクト登録画面</title>
    </head>

    <body>
        <div class="mainContents">
            <n:include path="/WEB-INF/view/common/menu.jsp" />
            <n:include path="/WEB-INF/view/common/header.jsp" />
        </div>
        <section>
            <n:form useToken="true" windowScopePrefixes="form">
                <div class="title-nav">
                    <span class="page-title">プロジェクト登録画面</span>
                </div>
                <%-- div class="message-area margin-top">
                    <n:errors filter="global" cssClass="message-error"/>
                </div --%>
                <h2 class="font-group">
                    プロジェクト情報
                </h2>
                <table class="table">
                    <tbody>
                        <tr>
                            <th class="width-250 required">
                                プロジェクト名
                            </th>
                            <td>
                                <div class="form-group">
                                    <n:text name="form.projectName" maxlength="100" cssClass="form-control width-300" errorCss="input-error" />
                                    <%-- n:error errorCss="message-error" name="form.projectName" / --%>
                                    <n:errors filter="all" errorCss="message-error" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="required">
                                プロジェクト種別
                            </th>
                            <td>
                                <div class="form-group">
                                    <%-- TODO④ Enum型のProjectTypeが持つ値をProjectType.values()で取得してリクエストスコープに設定する --%>
                                    <%-- requestスコープに設定されたprojectTypeをプルダウンで表示する --%>
                                    <n:select name="form.projectType"
                                              listName="projectTypeList"
                                              elementValueProperty="code"
                                              elementLabelProperty="label"
                                              elementLabelPattern="$LABEL$"
                                              cssClass="form-control"/>
                                    <n:error errorCss="message-error" name="form.projectType" />
                                </div>
                            </td>
                         </tr>
                         <tr>
                             <th class="required">
                                 プロジェクト分類
                             </th>
                             <td>
                                 <div class="form-group">
                                     <%-- ProjectClassをrequestスコープに設定する --%>
                                     <n:set var="projectClassList" value="<%= ProjectClass.values() %>"/>
                                     <%-- requestスコープに設定されたprojectClassをラジオボタンで表示する --%>
                                     <n:radioButtons name="form.projectClass"
                                         listName="projectClassList"
                                           elementValueProperty="code"
                                           elementLabelProperty="label"
                                           elementLabelPattern="$LABEL$"
                                           />
                                     <n:error errorCss="message-error" name="form.projectClass" />
                                 </div>
                             </td>
                          </tr>
                          <tr>
                             <th class="required">
                               性別
                             </th>
                             <td>
                                <div class="form-group">
                                   <%-- TODO③ codeSelectタグを利用して codeId=0001,pattern=PATTERN1を指定して、性別をプルダウンで表示する。TODO②の部分を削除する --%>
                                   <n:error errorCss="message-error" name="form.gender" />
                                </div>
                             </td>
                          </tr>
                    </tbody>
                </table>
                <div class="title-nav page-footer">
                    <div class="button-nav">
                        <n:forInputPage>
                            <div class="button-block real-button-block">

                                <n:submit uri="/action/trainingProject/confirmOfCreate" cssClass="btn btn-raised btn-success" value="登録">
                                    <%-- TODO② paramタグを利用して、form.genderに"1"の値を送るようにする --%>
                                </n:submit>
                            </div>
                            <div class="button-block link-button-block">
                                <n:a id="bottomBackLink" href="#" cssClass="btn btn-raised btn-default">戻る</n:a>
                            </div>
                        </n:forInputPage>
                        <n:forConfirmationPage>
                            <n:button uri="/action/trainingProject/top" cssClass="btn btn-raised btn-default">入力へ戻る</n:button>
                            <n:button uri="/action/trainingProject/create" cssClass="btn btn-raised btn-success" allowDoubleSubmission="false">確定</n:button>
                        </n:forConfirmationPage>
                    </div>
                </div>
            </n:form>
        </section>
        <n:include path="/WEB-INF/view/common/footer.jsp" />
        <n:forInputPage>
            <n:script type="text/javascript">
                $(function(){
                    setListUrlTo("topBackLink");
                    setListUrlTo("bottomBackLink");
                });
                $.material.init()
            </n:script>
        </n:forInputPage>
        <%-- 顧客検索 --%>
        <n:include path="/WEB-INF/view/client/index.jsp" />
    </body>
</html>
