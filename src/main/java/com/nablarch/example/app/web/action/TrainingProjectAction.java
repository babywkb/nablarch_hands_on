package com.nablarch.example.app.web.action;

import com.nablarch.example.app.web.form.TrainingProjectForm;
import nablarch.core.db.statement.SqlPStatement;
import nablarch.core.db.statement.SqlResultSet;
import nablarch.core.db.support.DbAccessSupport;
import nablarch.core.message.ApplicationException;
import nablarch.core.validation.ValidationContext;
import nablarch.core.validation.ValidationUtil;
import nablarch.fw.ExecutionContext;
import nablarch.fw.web.HttpRequest;
import nablarch.fw.web.HttpResponse;
import nablarch.fw.web.interceptor.OnError;

public class TrainingProjectAction extends DbAccessSupport {


    /**
     * プロジェクト登録初期画面を表示。
     *
     * @param request HTTPリクエスト
     * @param context 実行コンテキスト
     * @return HTTPレスポンス
     */
    public HttpResponse top(HttpRequest request, ExecutionContext context) {
        SqlPStatement sqlPStatement = getSqlPStatement("SELECT_PROJECTS");

        SqlResultSet resultSet = sqlPStatement.retrieve(0,5);
        context.setRequestScopedVar("projects", resultSet);

        return new HttpResponse("/WEB-INF/view/project/trainingCreate.jsp");
    }

    /**
     * 登録情報確認画面を表示。
     *
     * @param request HTTPリクエスト
     * @param context 実行コンテキスト
     * @return HTTPレスポンス
     */
    @OnError(type = ApplicationException.class, path = "forward://top")
    public HttpResponse confirmOfCreate(HttpRequest request, ExecutionContext context) {
        ValidationContext<TrainingProjectForm> validationContext
                = ValidationUtil.validateAndConvertRequest("form", TrainingProjectForm.class, request, "confirmation");
        validationContext.abortIfInvalid();

        TrainingProjectForm form = validationContext.createObject();
        context.setRequestScopedVar("form", form);
        return new HttpResponse("/WEB-INF/view/project/confirmOfTrainingCreate.jsp");
    }


    /**
     * 情報を登録する。
     *
     * @param request HTTPリクエスト
     * @param context 実行コンテキスト
     * @return HTTPレスポンス
     */
    @OnError(type = ApplicationException.class, path = "forward://top")
    public HttpResponse create(HttpRequest request, ExecutionContext context) {
        ValidationContext<TrainingProjectForm> validationContext
                = ValidationUtil.validateAndConvertRequest("form", TrainingProjectForm.class, request, "confirmation");
        validationContext.abortIfInvalid();
        TrainingProjectForm form = validationContext.createObject();


        return new HttpResponse("redirect://top");
    }
}

