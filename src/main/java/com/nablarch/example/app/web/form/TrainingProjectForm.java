package com.nablarch.example.app.web.form;

import nablarch.core.validation.PropertyName;
import nablarch.core.validation.ValidateFor;
import nablarch.core.validation.ValidationContext;
import nablarch.core.validation.ValidationUtil;
import nablarch.core.validation.validator.Length;

import java.io.Serializable;
import java.util.Map;

public class TrainingProjectForm implements Serializable {

    /** シリアルバージョンUID */
    private static final long serialVersionUID = 1L;

    /** プロジェクト名 */
    private String projectName;

    /** プロジェクト種別 */
    private String projectType;

    /** プロジェクト分類 */
    private String projectClass;

    /** プロジェクト分類 */
    private String gender;

    /**
     * コンストラクタ。
     *
     * @param params このクラスに設定するパラメータを持つMap
     */
    public TrainingProjectForm(Map<String, Object> params) {
        projectName = (String) params.get("projectName");
        projectType = (String) params.get("projectType");
        projectClass = (String) params.get("projectClass");
        gender = (String) params.get("gender");
    }

    /**
     * プロジェクト名を取得する。
     *
     * @return プロジェクト名
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * プロジェクト種別を取得する。
     *
     * @return プロジェクト種別
     */
    public String getProjectType() {
        return projectType;
    }

    /**
     * プロジェクト分類を取得する。
     *
     * @return プロジェクト分類
     */
    public String getProjectClass() {
        return projectClass;
    }

    /**
     * 性別を取得する。
     *
     * @return 性別
     */
    public String getGender() {
        return gender;
    }

    /**
     * プロジェクト名を設定する。
     *
     * @param projectName 設定するプロジェクト名
     */
    @Length(max = 64)
    @PropertyName("プロジェクト名")
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * プロジェクト種別を設定する。
     *
     * @param projectType 設定するプロジェクト種別
     */
    @PropertyName("プロジェクト種別")
    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    /**
     * プロジェクト分類を設定する。
     *
     * @param projectClass 設定するプロジェクト分類
     */
    @PropertyName("プロジェクト分類")
    public void setProjectClass(String projectClass) {
        this.projectClass = projectClass;
    }

    /**
     * プロジェクト分類を設定する。
     *
     * @param gender 設定する性別
     */
    @PropertyName("性別")
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 精査対象プロパティ。
     */
    private static final String[] KAKUNIN_GAMEN_TARGET = new String[]{"projectName", "projectType", "projectClass", "gender"};

    /**
     * 登録確認画面表示イベント時に実施するバリデーション。
     *
     * @param context バリデーションの実行に必要なコンテキスト
     */
    @ValidateFor("confirmation")
    public static void validateForSearchSelectItem(ValidationContext<TrainingProjectForm> context) {
        ValidationUtil.validate(context, KAKUNIN_GAMEN_TARGET);
    }
}
